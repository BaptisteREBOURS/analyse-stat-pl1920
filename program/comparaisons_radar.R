
#------------------------------------------------------------------------------------------

### MANAGE DATA ###

joueurs_radar <- joueurs_all_1920 %>%
  mutate(tklw_percent = tacles_reussis/tacles,
         int_90 = interceptions/min_90,
         recov_90 = ballons_recuperes/min_90,
         aerialw_percent = aeriens_gagnes/(aeriens_gagnes+aeriens_perdus),
         prgdist_90 = dist_avec_ballon_avant/min_90,
         prgpasse_90 = passes_reussies_avant/min_90,
         kp_90 = passes_cle/min_90,
         sca_90 = actions_avant_tir/min_90,
         crspa_90 = centres_surface/min_90,
         dribl_90 = dribles_reussis/min_90,
         sot_90 = tirs_cadres/min_90,
         inttklw_90 = (interceptions+tacles_reussis)/min_90,
         assist_90 = assists/min_90,
         buts_90 = buts/min_90,
         sot_percent = tirs_cadres/tirs,
         buts_xg = buts-exp_buts) %>%
  select(nom,club,nation,position,age,matchs_joues,matchs_titu,min_jouees,min_90,min_jouees_percent,
         tklw_percent,int_90,recov_90,inttklw_90,aerialw_percent,prgdist_90,prgpasse_90,kp_90,sca_90,crspa_90,dribl_90,sot_90,sot_percent,assist_90,buts_90,buts_xg) %>%
  filter(min_jouees_percent >= 25)


gardiens_radar <- gardiens_all_1920 %>%
  mutate(cs_percent = cleansheets_gk/min_90_gk,
         diff_psxg_ga = buts_expected_gk - buts_concedes_gk,
         degag_percent = degagements_reussis_gk/degagements_gk,
         cross_percent = centres_arretes_gk/centres_subis_gk,
         pen_percent = pen_arretes_gk/pen_joues_gk,
         kp_90 = passes_cle_gk/min_90_gk,
         acths_90 = actions_def_hors_surface_gk/min_90_gk) %>%
  select(nom_gk,club_gk,nation_gk,position_gk,age_gk,matchs_joues_gk,matchs_titu_gk,min_jouees_gk,min_90_gk,min_jouees_percent_gk,
         cs_percent,arrets_percent_gk,diff_psxg_ga,pen_percent,degag_percent,cross_percent,acths_90,kp_90) %>%
  filter(min_jouees_percent_gk >= 20)



dc_radar <- joueurs_radar %>% 
  filter(str_detect(position,"CB"))

lateraux_radar <- joueurs_radar %>%
  filter(str_detect(position,"FB"))

md_radar <- joueurs_radar %>%
  filter(str_detect(nom,"Georginio Wijnaldum") | str_detect(position,"DM") | (str_detect(position,"MF") & !str_detect(position,"AM") & !str_detect(position,"FW")))

mo_radar <- joueurs_radar %>%
  filter(str_detect(position,"AM") | nom=="Mason Mount")

ailiers_radar <- joueurs_radar %>%
  filter(((str_detect(position,"FW") | str_detect(position,"AM")) & (str_detect(position,"left") | str_detect(position,"right"))) | str_detect(position,"WM"))

att_radar <- joueurs_radar %>%
  filter(str_detect(position,"FW"))


#------------------------------------------------------------------------------------------

ailiers_sansna <- ailiers_radar[!is.na(ailiers_radar$dribl_90),]
ailiers_sansna$percentile <- ecdf(ailiers_sansna$dribl_90)(ailiers_sansna$dribl_90)

ailiers_sansna[ailiers_sansna$nom %in% c("Bertrand Traoré","Anwar El Ghazi","Ryan Fraser","Allan Saint-Maximin"),c("nom","percentile")]

#------------------------------------------------------------------------------------------
###########################################################################################


################### GK

### Mendy / Kepa
data_radar <- as.data.frame(matrix(c(50,0,2,61,54,56,86,93,67,92,88,62),ncol=6,byrow = T))
colnames(data_radar) <- c("Cleansheets %","Saves %","Diff. xG-goals","Clearing shots %","Crosses stopped %","Def.actions out. PA per 90")
rownames(data_radar) <- c("Kepa Arrizabalaga","Edouard Mendy")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,6) , rep(0,6) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' GKs (>20% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Kepa Arrizabalaga (Chelsea) vs Edouard Mendy (Stade Rennais)")
text(c(0,0),c(0.68,0.97),c("24%","38%"),cex=0.7,col=c(rgb(0.4,0,0.4,1)))


### Martinez / Heaton
data_radar <- as.data.frame(matrix(c(34,31,38,17,59,26,80,100,88,17,33,17),ncol=6,byrow = T))
colnames(data_radar) <- c("Cleansheets %","Saves %","Diff. xG-goals","Clearing shots %","Crosses stopped %","Def.actions out. PA per 90")
rownames(data_radar) <- c("Tom Heaton","Emiliano Martínez")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,6) , rep(0,6) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' GKs (>20% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Tom Heaton (Aston Villa) vs Emiliano Martínez (Arsenal)")



################### DC 

### Gabriel / Luiz
data_radar <- as.data.frame(matrix(c(48,50,2,48,89,46,35,14,68,88,10,73,84,81,79,35,60,55),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Aerials won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Goals per 90")
rownames(data_radar) <- c("David Luiz","Gabriel Magalhães")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("David Luiz (Arsenal) vs Gabriel Magalhães (LOSC)")




### Silva / Christensen
data_radar <- as.data.frame(matrix(c(12,90,29,48,86,29,59,53,43,60,36,67,40,98,90,5,72,43),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Aerials won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Goals per 90")
rownames(data_radar) <- c("Andreas Christensen","Thiago Silva")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Thiago Silva (PSG) vs Andreas Christensen (Chelsea)")



### Silva / Zouma
data_radar <- as.data.frame(matrix(c(18,99,88,96,85,59,7,50,43,60,36,67,40,98,90,5,72,43),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Aerials won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Goals per 90")
rownames(data_radar) <- c("Kurt Zouma","Thiago Silva")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Kurt Zouma (Chelsea) vs Thiago Silva (PSG)")




### Sarr / Zouma
data_radar <- as.data.frame(matrix(c(18,99,88,96,85,59,7,50,43,0,85,48,33,93,84,33,37,66),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Aerials won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Goals per 90")
rownames(data_radar) <- c("Kurt Zouma","Malang Sarr")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Malang Sarr (OGC Nice) vs Kurt Zouma (Chelsea)")



### Sarr / Christensen
data_radar <- as.data.frame(matrix(c(12,90,29,48,86,29,59,53,43,0,85,48,33,93,84,33,37,66),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Aerials won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Goals per 90")
rownames(data_radar) <- c("Andreas Christensen","Malang Sarr")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Malang Sarr (OGC Nice) vs Andreas Christensen (Chelsea)")



### Aké / Otamendi
data_radar <- as.data.frame(matrix(c(3,67,29,13,41,25,39,41,74,25,53,21,75,98,25,45,36,87),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Aerials won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Goals per 90")
rownames(data_radar) <- c("Nicolás Otamendi","Nathan Aké")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Nathan Aké (Bournemouth AFC) vs Nicolás Otamendi (Manchester City)")




################### Latéral

### Chilwell / Alonso
data_radar <- as.data.frame(matrix(c(92,75,92,86,73,94,94,93,97,21,63,51,99,51,90,86,60,88),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Goals per 90")
rownames(data_radar) <- c("Marcos Alonso","Ben Chilwell")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Ben Chilwell (Leicester) vs Marcos Alonso (Chelsea)")




### Castagne / Pereira
data_radar <- as.data.frame(matrix(c(35,88,29,95,82,67,79,43,87,52,17,28,42,45,84,83,79,67),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Goals per 90")
rownames(data_radar) <- c("Ricardo Pereira","Timothy Castagne")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Timothy Castagne (Atalanta) vs Ricardo Pereira (Leicester)")




### Doherty / Aurier
data_radar <- as.data.frame(matrix(c(37,86,82,35,39,70,57,71,54,20,72,28,58,79,47,73,39,89),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Goals per 90")
rownames(data_radar) <- c("Serge Aurier","Matt Doherty")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Matt Doherty (Wolverhampton) vs Serge Aurier (Tottenham)")



### Reguilon / Davies
data_radar <- as.data.frame(matrix(c(9,88,78,31,44,37,26,51,20,47,30,60,96,67,91,85,86,75),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Goals per 90")
rownames(data_radar) <- c("Ben Davies","Sergio Reguilón")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Ben Davies (Tottenham) vs Sergio Reguilón (Sevilla)")



### Marçal / Otto
data_radar <- as.data.frame(matrix(c(66,91,17,63,16,33,53,24,72,27,89,99,93,90,86,63,87,50),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Recoveries per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Goals per 90")
rownames(data_radar) <- c("Jonny Castro","Fernando Marçal")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FBs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Jonny Castro (Wolverhampton) vs Fernando Marçal (Olympique Lyonnais)")



################### CM

### Doucouré / André Gomes
data_radar <- as.data.frame(matrix(c(92,9,38,43,54,43,50,10,38,79,52,46,49,21,58,70,69,77),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Aerial won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Shots on target per 90","Goals per 90")
rownames(data_radar) <- c("André Gomes","Abdoulaye Doucouré")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CMs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Abdoulaye Doucouré (Watford) vs André Gomes (Everton)")




### Doucouré / Tom Davies
data_radar <- as.data.frame(matrix(c(8,87,55,27,57,10,21,52,48,79,52,46,49,21,58,70,69,77),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Aerial won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Shots on target per 90","Goals per 90")
rownames(data_radar) <- c("Tom Davies","Abdoulaye Doucouré")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CMs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Abdoulaye Doucouré (Watford) vs Tom Davies (Everton)")




### Allan / André Gomes
data_radar <- as.data.frame(matrix(c(92,9,38,43,54,43,50,10,38,61,4,77,61,42,71,82,84,83),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Aerial won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Shots on target per 90","Goals per 90")
rownames(data_radar) <- c("André Gomes","Allan")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CMs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("André Gomes (Everton) vs Allan (Napoli)")




### Allan / Tom Davies
data_radar <- as.data.frame(matrix(c(8,87,55,27,57,10,21,52,48,61,4,77,61,42,71,82,84,83),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Aerial won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Shots on target per 90","Goals per 90")
rownames(data_radar) <- c("Tom Davies","Allan")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CMs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Allan (Napoli) vs Tom Davies (Everton)")



### Thiago / Wijnaldum
data_radar <- as.data.frame(matrix(c(96,16,26,47,21,29,33,82,79,81,72,31,88,99,70,83,78,85),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Aerial won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Shots on target per 90","Goals per 90")
rownames(data_radar) <- c("Georginio Wijnaldum","Thiago Alcántara")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CMs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Georginio Wijnaldum (Liverpool) vs Thiago Alcántara (Bayern Munich)")



### Hojbjerg / Winks
data_radar <- as.data.frame(matrix(c(17,87,85,77,74,42,36,18,38,51,86,62,44,51,62,61,67,38),ncol=9,byrow = T))
colnames(data_radar) <- c("Tackles won %","Interceptions per 90","Aerial won %","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Shots on target per 90","Goals per 90")
rownames(data_radar) <- c("Harry Winks","Pierre Højbjerg")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' CMs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Pierre Højbjerg (Southampton) vs Harry Winks (Tottenham)")




################### MO

### Havertz / Mount
data_radar <- as.data.frame(matrix(c(40,48,47,62,68,47,42,78,60,8,71,46,73,87,67,73,79,89),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Assists per 90","Dribbles per 90","Shots on target per 90","Goals per 90")
rownames(data_radar) <- c("Mason Mount","Kai Havertz")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' AMs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Mason Mount (Chelsea) vs Kai Havertz (Bayer Leverkusen)")




################### Ailiers

### Willian / Pépé
data_radar <- as.data.frame(matrix(c(23,86,25,74,60,62,82,93,65,51,93,79,92,94,65,75,76,79),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Assists per 90","Dribbles per 90","Goals per 90")
rownames(data_radar) <- c("Nicolas Pépé","Willian")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' wingers (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Nicolas Pépé (Arsenal) vs Willian (Chelsea)")




### Ferrán Torres / Bernardo Silva
data_radar <- as.data.frame(matrix(c(83,88,55,88,87,61,88,66,73,19,52,28,37,32,83,69,79,52),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Assists per 90","Dribbles per 90","Goals per 90")
rownames(data_radar) <- c("Bernardo Silva","Ferrán Torres")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' wingers (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Ferrán Torres (Valencia) vs Bernardo Silva (Manchester City)")



### Traoré / El Ghazi
data_radar <- as.data.frame(matrix(c(14,36,19,51,44,88,62,53,56,79,53,69,77,79,88,72,83,31),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Assists per 90","Dribbles per 90","Goals per 90")
rownames(data_radar) <- c("Anwar El Ghazi","Bertrand Traoré")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' wingers (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Bertrand Traoré (Olympique Lyonnais) vs Anwar El Ghazi (Aston Villa)")



### Fraser / ASM
data_radar <- as.data.frame(matrix(c(15,97,9,68,88,44,66,99,50,18,61,74,78,57,51,62,7,19),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Prog. dist. per 90","Prog. passes per 90","Key passes per 90","SCA per 90","Crosses per 90","Assists per 90","Dribbles per 90","Goals per 90")
rownames(data_radar) <- c("Allan Saint-Maximin","Ryan Fraser")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' wingers (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Ryan Fraser (Bournemouth AFC) vs Allan Saint-Maximin (Newcastle United)")




################### Attaquant

### Werner / Giroud
data_radar <- as.data.frame(matrix(c(8,91,11,17,12,0,91,96,72,16,18,78,83,83,74,93,98,98),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Aerials won %","Key passes per 90","SCA per 90","Assists per 90","Dribbles per 90","Shots on target %","Goals per 90","Diff. goals-xG")
rownames(data_radar) <- c("Olivier Giroud","Timo Werner")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FWs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Olivier Giroud (Chelsea) vs Timo Werner (RB Leipzig)")




### Werner / Abraham
data_radar <- as.data.frame(matrix(c(18,56,15,21,48,23,62,91,59,16,18,78,83,83,74,93,98,98),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Aerials won %","Key passes per 90","SCA per 90","Assists per 90","Dribbles per 90","Shots on target %","Goals per 90","Diff. goals-xG")
rownames(data_radar) <- c("Tammy Abraham","Timo Werner")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FWs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Timo Werner (RB Leipzig) vs Tammy Abraham (Chelsea)")




### Wilson / Joelinton
data_radar <- as.data.frame(matrix(c(44,64,35,22,24,57,17,10,4,9,25,20,16,13,17,68,43,8),ncol=9,byrow = T))
colnames(data_radar) <- c("Int+Tackles won per 90","Aerials won %","Key passes per 90","SCA per 90","Assists per 90","Dribbles per 90","Shots on target %","Goals per 90","Diff. goals-xG")
rownames(data_radar) <- c("Joelinton","Callum Wilson")

# To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each variable to show on the plot!
data_radar <- rbind(rep(100,9) , rep(0,9) , data_radar)

# Color vector
colors_border=c(rgb(0.4,0,0.4,0.8),rgb(0.8,0.9,0,0.8))
colors_in=c(rgb(0.4,0,0.4,0.4),rgb(0.8,0.9,0,0.4))

radarchart(data_radar,axistype=6,pcol=colors_border,pfcol=colors_in,plwd=4,plty=1,
           cglcol="grey",cglty=1,axislabcol="grey",vlcex=0.75)

legend(x=1.3,y=1.2,legend=rownames(data_radar[-c(1,2),]),bty="n",pch=15,col=colors_in,text.col="darkgrey",cex=1,pt.cex=2)
mtext("Percentile data of the Europe's top 5 leagues' FWs (>25% of team's total minutes)", side=3)
mtext("Data source: StatsBomb",side=1,cex=0.75,at=2)
title("Callum Wilson (Bournemouth AFC) vs Joelinton (Newcastle United)")

