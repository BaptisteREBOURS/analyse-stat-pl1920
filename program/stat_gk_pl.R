
###### UNIVARIE ######

## GA/90
gardiens_pl %>%
  filter(!is.na(buts_concedes_gk) & min_jouees_percent_gk > 20) %>%
  mutate(buts_par_match = buts_concedes_gk/min_90_gk) %>%
  mutate(nom_gk = fct_reorder(nom_gk,desc(buts_par_match))) %>%
  ggplot(aes(x=nom_gk, y=buts_par_match)) +
  geom_bar(stat="identity", fill="#45004B", alpha=c(rep(0.5,11),.8,rep(0.5,2),.8,rep(0.5,11)), width=.6) +
  annotate(geom="text", x=24, y=0.98, label="0.9",size=4,vjust=0.4)+
  annotate(geom="text", x=1, y=2.56, label="2.5",size=4,vjust=0.3)+
  scale_y_continuous(labels = scales::number_format(accuracy = 0.1)) +
  coord_flip() + xlab("") + ylab("") + theme_bw() + ggtitle("Goals against per 90 min.") + 
  labs(subtitle = "Goalkeepers who played at least 20% of team's total minutes",caption = "Data source: StatsBomb") + 
  theme(panel.grid.major.y = element_blank(),panel.grid.minor.x = element_blank())

## CleanSheet%
gardiens_pl %>%
  filter(!is.na(cleansheets_gk) & min_jouees_percent_gk > 20) %>%
  mutate(cs_percent = cleansheets_gk/min_90_gk) %>%
  mutate(nom_gk = fct_reorder(nom_gk,cs_percent)) %>%
  ggplot(aes(x=nom_gk, y=cs_percent)) +
  geom_bar(stat="identity", fill="#45004B", alpha=c(0.5,0.8,rep(0.5,5),0.8,rep(0.5,2),rep(0.8,3),rep(0.5,13)), width=.6) +
  annotate(geom="text", x=1, y=0.13, label="11.1%",size=4,hjust=0.3,vjust=0.4)+
  annotate(geom="text", x=7, y=0.225, label="20.6%",size=4,hjust=0.3,vjust=0.3)+
  annotate(geom="text", x=22, y=0.37, label="35.0%",size=4,hjust=0.3,vjust=0.4)+
  annotate(geom="text", x=23, y=0.38, label="36.1%",size=4,hjust=0.3,vjust=0.3)+
  annotate(geom="text", x=24, y=0.415, label="39.5%",size=4,hjust=0.3,vjust=0.3)+
  coord_flip() + xlab("") + ylab("") + theme_bw() + ggtitle("Clean sheets percentage") +
  scale_y_continuous(labels = scales::percent,limits=c(0,0.5)) +
  labs(subtitle = "Goalkeepers who played at least 20% of team's total minutes",caption = "Data source: StatsBomb") + 
  theme(panel.grid.major.y = element_blank(),panel.grid.minor.x = element_blank())

## Save%
gardiens_pl %>%
  filter(!is.na(arrets_percent_gk) & min_jouees_percent_gk > 20) %>%
  mutate(nom_gk = fct_reorder(nom_gk,arrets_percent_gk)) %>%
  ggplot(aes(x=nom_gk, y=arrets_percent_gk)) +
  geom_bar(stat="identity", fill="#45004B", alpha=c(rep(0.5,4),.8,rep(0.5,4),rep(0.8,3),rep(0.5,14)), width=.6) +
  annotate(geom="text", x=1, y=0.57, label="54.5%",size=4,hjust=0.3,vjust=0.4)+
  annotate(geom="text", x=23, y=0.78, label="75.2%",size=4,hjust=0.3,vjust=0.3)+
  annotate(geom="text", x=24, y=0.81, label="77.6%",size=4,hjust=0.3,vjust=0.4)+
  annotate(geom="text", x=26, y=0.84, label="81.0%",size=4,hjust=0.3,vjust=0.3)+
  coord_flip() + xlab("") + ylab("") + theme_bw() + ggtitle("Save percentage") +
  scale_y_continuous(labels = scales::percent_format(accuracy=0.1),limits=c(0,0.85)) +
  labs(subtitle = "Goalkeepers who played at least 20% of team's total minutes",caption = "Data source: StatsBomb") + 
  theme(panel.grid.major.y = element_blank(),panel.grid.minor.x = element_blank())




## PSxG - GA
gardiens_pl %>%
  filter(!is.na(buts_expected_gk) & min_jouees_percent_gk > 20) %>%
  mutate(diff_psxg_ga = buts_expected_gk - buts_concedes_gk,
         nom_gk = fct_reorder(nom_gk,diff_psxg_ga),
         type_diff = ifelse(diff_psxg_ga > 0,"upper","lower")) %>%
  ggplot(aes(x=nom_gk, y=diff_psxg_ga,fill=diff_psxg_ga)) +
  geom_bar(stat="identity", alpha=.8, width=.6) +
  annotate(geom="text", x=26, y=8.9, label="+8.0",size=4,vjust=0.4)+
  annotate(geom="text", x=1, y=-13, label="-11.9",size=4,vjust=0.3)+
  annotate(geom="text", x=2, y=-8.9, label="-8.1",size=4,vjust=0.3)+
  annotate(geom="text", x=3, y=-8.5, label="-7.6",size=4,vjust=0.3)+
  annotate(geom="text", x=25, y=7.3, label="+6.5",size=4,vjust=0.3)+
  annotate(geom="text", x=24, y=7.1, label="+6.3",size=4,vjust=0.3)+
  coord_flip() + xlab("") + ylab("") + theme_bw() + ggtitle("Difference between post-shot expected goals and allowed goals") +
  scale_fill_gradient2(low='red', mid='darkgrey', high='forestgreen') +
  theme(legend.position = "none",panel.grid.major.y = element_blank(),panel.grid.minor.x = element_blank()) + scale_y_continuous(labels=scales::number_format(accuracy=0.1),limits = c(-15,10)) + 
  labs(subtitle="Goalkeepers who played at least 20% of team's total minutes",caption = "Data source: StatsBomb")



###### BIVARIE ######

## Save% x SoTA/90
gardiens_pl %>%
  filter(!is.na(arrets_percent_gk) & !is.na(tirs_cadres_concedes_gk) & min_jouees_percent_gk > 20) %>%
  mutate(tirs_cadres_parmatch = tirs_cadres_concedes_gk/min_90_gk,
         nom_gk = ifelse(nom_gk %in% c("Hugo Lloris","Emiliano Mart�nez","Bernd Leno"),nom_gk,"")) %>%
  ggplot(aes(x=tirs_cadres_parmatch, y=arrets_percent_gk,label=nom_gk)) + 
  geom_point(fill=c(rep("darkgrey",6),"#74007D",rep("darkgrey",2),rep("#74007D",2),rep("darkgrey",15)),shape=21,size=c(rep(3,6),4,rep(3,2),rep(4,2),rep(3,15))) +
  xlab("Tirs cadr�s par match") + ylab("Pourcentage de tirs arr�t�s") +
  scale_y_continuous(labels = scales::percent_format(accuracy=0.1),breaks=c(0.5,0.6,0.7,0.8,0.9),limits=c(0.5,0.85)) +
  theme_bw() + scale_x_continuous(limits=c(2.5,6),labels=scales::number_format(accuracy=0.1)) +
  geom_text_repel(segment.alpha = 0.2,box.padding = 0.4) + 
  ggtitle("Most requested and efficient goalkeepers") +
  labs(subtitle = "Goalkeepers who played at least 20% of team's total minutes",caption = "Data source: StatsBomb") + 
  theme(panel.grid.minor.y = element_blank(),panel.grid.minor.x = element_blank()) +
  xlab("Shots on target against per 90 min.") + ylab("Save percentage")



## Launched% x Save%
gardiens_pl %>%
  filter(!is.na(degagements_reussis_gk) & !is.na(arrets_percent_gk) & min_jouees_percent_gk > 20) %>%
  mutate(degag_percent = degagements_reussis_gk/degagements_gk,
         nom_gk = ifelse(nom_gk %in% c("Angus Gunn","Kasper Schmeichel"),nom_gk,"")) %>%
  ggplot(aes(x=arrets_percent_gk, y=degag_percent,label=nom_gk)) + 
  geom_point(fill=c(rep("darkgrey",5),"#74007D",rep("darkgrey",8),"#74007D",rep("darkgrey",11)),shape=21,size=c(rep(3,5),4,rep(3,8),4,rep(3,11))) +
  scale_y_continuous(labels = scales::percent_format(accuracy=0.1),limits=c(0.225,0.55)) +
  scale_x_continuous(labels = scales::percent_format(accuracy=0.1),limits=c(0.525,0.825)) +
  theme_bw() + ggtitle("Save percentage x Completed clearing shots percentage") +
  geom_text_repel(segment.alpha = 0.2,box.padding = 0.4) +   labs(subtitle = "Goalkeepers who played at least 20% of team's total minutes",caption = "Data source: StatsBomb") + 
  theme(panel.grid.minor.y = element_blank(),panel.grid.minor.x = element_blank()) +
  xlab("Save percentage") + ylab("Completed clearing shots percentage")




###### MULTI ######

gardiens_multi <- gardiens_pl %>%
  filter(!is.na(cleansheets_gk) & !is.na(arrets_percent_gk) & !is.na(centres_arretes_gk) & !is.na(degagements_reussis_gk))

spyder <- c()
for (i in 1:nrow(gardiens_multi)){
  if (i %in% c(1,2)){
    spyder_new <- gardiens_multi[i,] %>%
      mutate(cs_percent = cleansheets_gk/matchs_joues_gk,
             centres_percent = centres_arretes_gk/centres_subis_gk,
             degagements_percent = degagements_reussis_gk/degagements_gk) %>%
      select(nom_gk,arrets_percent_gk,cs_percent,centres_percent,degagements_percent) %>%
      rename('Save%' = arrets_percent_gk,'Cleansheet%' = cs_percent,'Crosses%' = centres_percent,'Launches%' = degagements_percent)
    rownames(spyder_new) <- spyder_new$nom_gk
    spyder_new <- spyder_new %>% select(-nom_gk)
    spyder <- rbind(spyder,spyder_new)
  }
}

spyder <- rbind(rep(1,5),rep(0,5),spyder)

colors_border=c( rgb(0.2,0.5,0.5,0.9),rgb(0.8,0.2,0.5,0.9))
colors_in=c(rgb(0.2,0.5,0.5,0.4),rgb(0.8,0.2,0.5,0.4))

radarchart(spyder,axistype=1,seg=4,pcol=colors_border,pfcol=colors_in,plwd=3,plty=1,calcex=0.7,
          cglcol="grey", cglty=1, axislabcol="grey",cglwd=0.1,vlcex=0.9,title = "Liverpool goalkeepers")

legend(x=1,y=1.2,legend=rownames(spyder[-c(1,2),]),bty="n",pch=20,col=colors_in,text.col="grey",cex=1,pt.cex=3)







gardiens_pl %>%
  filter(!is.na(centres_arretes_gk) & min_jouees_percent_gk > 20) %>%
  mutate(centres_pct = centres_arretes_gk / centres_subis_gk) %>%
  summarize(moy_cen = mean(centres_pct))

