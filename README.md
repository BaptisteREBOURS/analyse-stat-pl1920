# ANALYSE STAT PREMIER LEAGUE SAISON 2019-20

Données web scrapées sur fbref.com (StatsBomb)
Analyses réalisés sur R, graphiques avec ggplot2 et fmsb (radar chart)

L'idée est de comparer les performances des gardiens et des attaquants sur la saison 19/20, selon plusieurs statistiques importantes pour chacun de ces deux postes. J'ai également comparer les performances de certains joueurs arrivant en Angleterre avec celles du joueur occupant le même poste dans le club d'arrivée.

Les packages nécessaires pour reproduire le projet se trouvent dans le fichier _1_param.R_
Le code pour effectuer le web scraping (avec R) est disponible dans le fichier _2_get_data.R_

Les analyses finales sont disponibles dans le dossier _publications_
